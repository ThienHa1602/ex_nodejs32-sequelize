import express from "express";
import cors from "cors";
import rootRouter from "./routers/rootRouter.js";

const app = express();

app.use(express.json()); // middle ware giúp BE đọc được cấu trúc JSON
app.use(cors()); // middle ware chấp nhận tất cả domain FE truy cập vào BE này
app.listen(8080); // tạo sever BE với port = 8080
app.use("/api", rootRouter);
