import { successCode } from "../config/response.js";
import sequelize from "../models/index.js";
import initModels from "../models/init-models.js";

const models = initModels(sequelize);

const order = async (req, res) => {
  try {
    let { user_id, food_id, amount, code, arr_sub_id } = req.body;
    let newData = { user_id, food_id, amount, code, arr_sub_id };
    await models.order.create(newData);
    successCode(res, newData, "Order thành công");
  } catch (error) {
    res.status(500).send(error.message);
  }
};
export { order };
