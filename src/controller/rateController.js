import { successCode } from "../config/response.js";
import sequelize from "../models/index.js";
import initModels from "../models/init-models.js";

const models = initModels(sequelize);

const rate = async (req, res) => {
  try {
    let { user_id, res_id } = req.body;
    let newData = {
      user_id,
      res_id,
      date_rate: Date.now(),
    };
    await models.rate_res.create(newData);
    successCode(res, newData, "Đánh giá thành công");
  } catch (error) {
    res.status(500).send(error.message);
  }
};
const getRateRes = async (req, res) => {
  try {
    let { res_id } = req.params;
    let data = await models.rate_res.findAll({
      where: { res_id },
      include: ["user"],
    });
    successCode(res, data, "Danh sách đánh giá theo nhà hàng");
  } catch (error) {
    res.status(500).send(error.message);
  }
};
const getRateUSer = async (req, res) => {
  try {
    let { user_id } = req.params;
    let data = await models.rate_res.findAll({
      where: { user_id },
      include: ["re"],
    });
    successCode(res, data, "Danh sách đánh giá theo User");
  } catch (error) {
    res.status(500).send(error.message);
  }
};

export { rate, getRateRes, getRateUSer };
