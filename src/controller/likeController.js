import { successCode } from "../config/response.js";
import sequelize from "../models/index.js";
import initModels from "../models/init-models.js";

const models = initModels(sequelize);

const like = async (req, res) => {
  try {
    let { user_id, res_id } = req.body;
    let newData = {
      user_id,
      res_id,
      date_like: Date.now(),
    };
    await models.like_res.create(newData);
    successCode(res, newData, "Thêm mới thành công");
  } catch (error) {
    res.status(500).send(error.message);
  }
};

const unlike = async (req, res) => {
  try {
    let { user_id, res_id } = req.params;
    await models.like_res.destroy({ where: { user_id, res_id } });
    successCode(res, "", "Đã unlike");
  } catch (error) {
    res.status(500).send(error.message);
  }
};

const getLikeRes = async (req, res) => {
  try {
    let { res_id } = req.params;
    let data = await models.like_res.findAll({
      where: { res_id },
      include: ["user"],
    });
    successCode(res, data, "Danh sách like theo nhà hàng");
  } catch (error) {
    res.status(500).send(error.message);
  }
};

const getLikeUser = async (req, res) => {
  try {
    let { user_id } = req.params;
    let data = await models.like_res.findAll({
      where: { user_id },
      include: ["re"],
    });
    successCode(res, data, "Danh sách like theo User");
  } catch (error) {
    res.status(500).send(error.message);
  }
};
export { like, unlike, getLikeRes, getLikeUser };
