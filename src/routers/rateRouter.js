import express from "express";
import { getRateRes, getRateUSer, rate } from "../controller/rateController.js";

const rateRouter = express.Router();

rateRouter.post("/rate", rate);
rateRouter.get("/rate/res/:res_id", getRateRes);
rateRouter.get("/rate/user/:user_id", getRateUSer);
export default rateRouter;
