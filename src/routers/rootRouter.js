import express from "express";
import likeRouter from "./likeRouter.js";
import rateRouter from "./rateRouter.js";
import orderRouter from "./orderRouter.js";

const rootRouter = express.Router();

rootRouter.use("/like-res", likeRouter);
rootRouter.use("/rate-res", rateRouter);
rootRouter.use("/order", orderRouter);

export default rootRouter;
