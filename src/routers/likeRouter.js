import express from "express";
import {
  getLikeRes,
  getLikeUser,
  like,
  unlike,
} from "../controller/likeController.js";

const likeRouter = express.Router();

likeRouter.post("/like", like);
likeRouter.delete("/like/:user_id/:res_id", unlike);
likeRouter.get("/like/res/:res_id", getLikeRes);
likeRouter.get("/like/user/:user_id", getLikeUser);

export default likeRouter;
