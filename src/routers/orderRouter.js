import express from "express";
import { order } from "../controller/orderController.js";

const orderRouter = express.Router();

orderRouter.post("/", order);
export default orderRouter;
