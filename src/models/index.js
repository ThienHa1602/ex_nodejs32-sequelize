import { Sequelize } from "sequelize";
import config from "../config/config.js";

const sequelize = new Sequelize(config.database, config.user, config.pass, {
  host: config.host,
  dialect: config.dialect,
  port: config.port,
});

try {
  await sequelize.authenticate();
  console.log("Thanh cong");
} catch (error) {
  console.log("That bai");
}

export default sequelize;
